package steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import org.testng.Assert;
import pages.MyTasksPage;

public class MyTasksSteps {
    MyTasksPage myTasksPage = new MyTasksPage();

    @And ("press the Enter key")
    public void pressTheEnterKey() {
        myTasksPage.pressEnterKey();
    }

    @And ("clicks on Add button")
    public void clickAddButton() {
        myTasksPage.clickAddButton();
    }

    @Then("the task should be appended on the list of created tasks")
    public void taskAppended() {
        Assert.assertTrue(myTasksPage.taskCreated(), "Failure! Task not created");
    }

    @And ("type a task name with (.*) characters")
    public void typeATaskName(int size) {
        myTasksPage.enterTaskName(size);
    }

    @Then ("task should not be created successfully")
    public void taskShouldNotBeCreated() {
        Assert.assertTrue(myTasksPage.taskNotCreated(), "Failure! Task was created");
    }

    @Then ("should see a message (.*) on the top part")
    public void messageForToday(String message) {
        Assert.assertTrue(myTasksPage.messageOnTopPart(message), "Failure! Wrong message on top part");
    }

    @And ("clicks on Manage Subtasks button")
    public void manageSubtasks() {
        myTasksPage.clickOnManageSubtasks();
    }
}
