package steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import pages.HomePage;
import utils.PropertiesUtil;

public class HomePageSteps {
    HomePage homePage = new HomePage();

    @Given ("the user is logged into the system")
    public void signIn() {
        homePage.signIn(PropertiesUtil.getValue("validUser"),
                        PropertiesUtil.getValue("validPassword"));
    }

    @When ("clicks on My tasks link")
    public void clickMyTasks() {
        homePage.clickOnMyTasks();
    }
}
