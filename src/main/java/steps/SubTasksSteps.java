package steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.testng.Assert;
import pages.SubTasksPage;

public class SubTasksSteps {
    SubTasksPage subtasksPage = new SubTasksPage();
    public String dueDate = "";

    @And ("type a subtask description with (.*) characters")
    public void subtaskDescription(int size) {
        subtasksPage.subTaskName(size);
    }

    @And ("type due date")
    public void subtaskDueDate() {
        this.dueDate = "01/01/2020";
        subtasksPage.typeDueDate(dueDate);
    }

    @When("clicks on subtask 'Add' button")
    public void addSubtask() {
        subtasksPage.clickAddButton();
    }

    @Then ("the subtask should be appended at the bottom part of the modal dialog")
    public void subtaskAppended() {
        Assert.assertTrue(subtasksPage.subtaskCreated(), "Failure! Subtask not created");
    }

    @Then ("subtask should not be created successfully")
    public void subtaskNotCreated() {
        Assert.assertTrue(subtasksPage.subtaskNotCreated(), "Failure! Subtask was created");
    }

    @And ("don't fill description and erase due date")
    public void descriptionDueDateEmpty() {
        subtasksPage.eraseDueDate();
    }

    @Then("empty subtask should not be created successfully")
    public void emptySubtaskNotCreated() {
        Assert.assertTrue(subtasksPage.emptySubtaskNotCreated(), "Failure! Subtask was created");
    }

    @And ("fill incorrect Due Date")
    public void incorrectDueDate() {
        this.dueDate = "33/33/2018";
        subtasksPage.eraseDueDate();
        subtasksPage.typeDueDate(dueDate);
    }

    @And ("the user clicks on Close button")
    public void clickOnCloseButton(){
        subtasksPage.clickCloseButton();
    }
}
