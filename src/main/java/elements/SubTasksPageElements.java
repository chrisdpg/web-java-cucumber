package elements;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.Utils;

import java.util.List;

public class SubTasksPageElements extends Utils {
    @FindBy(id = "new_sub_task")
    protected WebElement description;

    @FindBy(id = "dueDate")
    protected WebElement dueDate;

    @FindBy(id = "add-subtask")
    protected WebElement add;

    @FindBy(css = "[ng-click='close()']")
    protected WebElement close;

    @FindBy(css = "[editable-text='sub_task.body']")
    protected List<WebElement> subtasksCreated;
}
