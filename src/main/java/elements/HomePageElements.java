package elements;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.Utils;

public class HomePageElements extends Utils {
    @FindBy(css = "ul.nav.navbar-nav.navbar-right li:nth-child(1) a")
    protected WebElement signIn;

    @FindBy(id = "user_email")
    protected WebElement email;

    @FindBy(id = "user_password")
    protected WebElement password;

    @FindBy(css = ".btn[type='submit']")
    protected WebElement enterBtn;

    @FindBy(css = "[href='/tasks']")
    protected WebElement myTasks;
}
