package elements;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.Utils;

import java.util.List;

public class MyTasksPageElements extends Utils {
    @FindBy(id = "new_task")
    protected WebElement taskDescription;

    @FindBy(css = "[ng-click='addTask()']")
    protected WebElement addTask;

    @FindBy(css = "[ng-click='editModal(task)']")
    protected WebElement manageSubtasks;

    @FindBy(css = "[editable-text='task.body']")
    protected List<WebElement> tasksCreated;

    @FindBy(css = "div h1")
    protected WebElement message;
}
