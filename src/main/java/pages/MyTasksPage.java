package pages;

import elements.MyTasksPageElements;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import setup.Setup;
import utils.Utils;

public class MyTasksPage extends MyTasksPageElements {

    public static String taskName = "";

    public MyTasksPage() {
        driver = Setup.getDriver();
        PageFactory.initElements(Setup.getDriver(), this);
    }

    public void pressEnterKey(){
        taskDescription.sendKeys(Keys.ENTER);
    }

    public void clickAddButton() {
        addTask.click();
    }

    public void clickOnManageSubtasks() {
        manageSubtasks.click();
    }

    public void enterTaskName(int size) {
        this.taskName = Utils.getRandomString(size);
        taskDescription.sendKeys(this.taskName);
    }

    public boolean taskCreated() {
        int cont = 0;
        for(WebElement item : tasksCreated) {
            if (item.getText().equals(getTaskName())) {
                cont++;
            }
        }
        return cont > 0;
    }

    public boolean taskNotCreated() {
        int cont = 0;
        for(WebElement item : tasksCreated) {
            if (item.getText().equals(getTaskName())) {
                cont++;
            }
        }
        return cont == 0;
    }

    public boolean messageOnTopPart(String msg) {
        return message.getText().equals(msg);
    }

    public String getTaskName(){
        return this.taskName;
    }
}
