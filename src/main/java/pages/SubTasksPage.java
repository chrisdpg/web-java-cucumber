package pages;

import elements.SubTasksPageElements;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import setup.Setup;
import utils.Utils;

public class SubTasksPage extends SubTasksPageElements {

    String subtaskName = "";

    public SubTasksPage() {
        driver = Setup.getDriver();
        PageFactory.initElements(Setup.getDriver(), this);
    }

    public void subTaskName(int size) {
        this.subtaskName = Utils.getRandomString(size);
        description.sendKeys(subtaskName);
    }

    public void typeDueDate(String date) {
        dueDate.sendKeys(date);
    }

    public void clickAddButton() {
        Utils.waitElementToBeClickable(add);
        add.click();
    }

    public boolean subtaskCreated() {
        int cont = 0;
        for(WebElement item : subtasksCreated) {
            if (item.getText().equals(getTaskName())) {
                cont++;
            }
        }
        return cont > 0;
    }

    public boolean subtaskNotCreated() {
        int cont = 0;
        for(WebElement item : subtasksCreated) {
            if (item.getText().equals(getTaskName())) {
                cont++;
            }
        }
        return cont == 0;
    }

    public boolean emptySubtaskNotCreated() {
        int cont = 0;
        for(WebElement item : subtasksCreated) {
            if (item.getText().equals("empty")) {
                cont++;
            }
        }
        return cont == 0;
    }

    public void eraseDueDate() {
        dueDate.clear();
    }

    public String getTaskName(){
        return this.subtaskName;
    }

    public void clickCloseButton(){
        close.click();
    }
}
