package pages;

import elements.HomePageElements;
import org.openqa.selenium.support.PageFactory;
import setup.Setup;
import utils.Utils;

public class HomePage extends HomePageElements {

    public HomePage() {
        driver = Setup.getDriver();
        PageFactory.initElements(Setup.getDriver(), this);
    }

    public void signIn(String mail, String pwd) {
        signIn.click();
        email.sendKeys(mail);
        password.sendKeys(pwd);
        enterBtn.click();
    }

    public void clickOnMyTasks() {
        Utils.waitElementToBeClickable(myTasks);
        myTasks.click();
    }
}
