Feature: Create Subtask
  Validating requirements from US#2

  Scenario: Create a new subtask
    Given the user is logged into the system
    When clicks on My tasks link
    And clicks on Manage Subtasks button
    And type a subtask description with 5 characters
    And type due date
    When clicks on subtask 'Add' button
    Then the subtask should be appended at the bottom part of the modal dialog
    And the user clicks on Close button

  Scenario: Create a new subtask with more than 250 characters in the name
    Given the user is logged into the system
    When clicks on My tasks link
    And clicks on Manage Subtasks button
    And type a subtask description with 251 characters
    And clicks on subtask 'Add' button
    Then subtask should not be created successfully
    And the user clicks on Close button

  Scenario: Create a new subtask without description and due date
    Given the user is logged into the system
    When clicks on My tasks link
    And clicks on Manage Subtasks button
    And don't fill description and erase due date
    When clicks on subtask 'Add' button
    Then empty subtask should not be created successfully
    And the user clicks on Close button

  Scenario: Create a new subtask with incorrect Due Date
    Given the user is logged into the system
    When clicks on My tasks link
    And clicks on Manage Subtasks button
    And type a subtask description with 5 characters
    And fill incorrect Due Date
    When clicks on subtask 'Add' button
    Then subtask should not be created successfully
    And the user clicks on Close button