Feature: Create Task
  Validating requirements from US#1

  Scenario: Create a new task hitting Enter
    Given the user is logged into the system
    When clicks on My tasks link
    And type a task name with 10 characters
    And press the Enter key
    Then the task should be appended on the list of created tasks

  Scenario: Create a new task by clicking Add button
    Given the user is logged into the system
    When clicks on My tasks link
    And type a task name with 10 characters
    And clicks on Add button
    Then the task should be appended on the list of created tasks

  Scenario: Create a new task with less than 3 characters in the name
    Given the user is logged into the system
    When clicks on My tasks link
    And type a task name with 2 characters
    And clicks on Add button
    Then task should not be created successfully

  Scenario: Create a new task with more than 250 characters in the name
    Given the user is logged into the system
    When clicks on My tasks link
    And type a task name with 251 characters
    And clicks on Add button
    Then task should not be created successfully

  Scenario: Validate the message on the top part
    Given the user is logged into the system
    When clicks on My tasks link
    Then should see a message Hey Christian, this is your todo list for today:' on the top part