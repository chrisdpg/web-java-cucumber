$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/CreateSubTask.feature");
formatter.feature({
  "name": "Create Subtask",
  "description": "  Validating requirements from US#2",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Create a new subtask",
  "description": "",
  "keyword": "Scenario"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "the user is logged into the system",
  "keyword": "Given "
});
formatter.match({
  "location": "HomePageSteps.signIn()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "clicks on My tasks link",
  "keyword": "When "
});
formatter.match({
  "location": "HomePageSteps.clickMyTasks()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "clicks on Manage Subtasks button",
  "keyword": "And "
});
formatter.match({
  "location": "MyTasksSteps.manageSubtasks()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "type a subtask description with 5 characters",
  "keyword": "And "
});
formatter.match({
  "location": "SubTasksSteps.subtaskDescription(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "type due date",
  "keyword": "And "
});
formatter.match({
  "location": "SubTasksSteps.subtaskDueDate()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "clicks on subtask \u0027Add\u0027 button",
  "keyword": "When "
});
formatter.match({
  "location": "SubTasksSteps.addSubtask()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the subtask should be appended at the bottom part of the modal dialog",
  "keyword": "Then "
});
formatter.match({
  "location": "SubTasksSteps.subtaskAppended()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user clicks on Close button",
  "keyword": "And "
});
formatter.match({
  "location": "SubTasksSteps.clickOnCloseButton()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Create a new subtask with more than 250 characters in the name",
  "description": "",
  "keyword": "Scenario"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "the user is logged into the system",
  "keyword": "Given "
});
formatter.match({
  "location": "HomePageSteps.signIn()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "clicks on My tasks link",
  "keyword": "When "
});
formatter.match({
  "location": "HomePageSteps.clickMyTasks()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "clicks on Manage Subtasks button",
  "keyword": "And "
});
formatter.match({
  "location": "MyTasksSteps.manageSubtasks()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "type a subtask description with 251 characters",
  "keyword": "And "
});
formatter.match({
  "location": "SubTasksSteps.subtaskDescription(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "clicks on subtask \u0027Add\u0027 button",
  "keyword": "And "
});
formatter.match({
  "location": "SubTasksSteps.addSubtask()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "subtask should not be created successfully",
  "keyword": "Then "
});
formatter.match({
  "location": "SubTasksSteps.subtaskNotCreated()"
});
formatter.result({
  "error_message": "java.lang.AssertionError: Failure! Subtask was created expected [true] but found [false]\n\tat org.testng.Assert.fail(Assert.java:96)\n\tat org.testng.Assert.failNotEquals(Assert.java:776)\n\tat org.testng.Assert.assertTrue(Assert.java:44)\n\tat steps.SubTasksSteps.subtaskNotCreated(SubTasksSteps.java:36)\n\tat ✽.subtask should not be created successfully(file:src/test/resources/features/CreateSubTask.feature:20)\n",
  "status": "failed"
});
formatter.step({
  "name": "the user clicks on Close button",
  "keyword": "And "
});
formatter.match({
  "location": "SubTasksSteps.clickOnCloseButton()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Create a new subtask without description and due date",
  "description": "",
  "keyword": "Scenario"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "the user is logged into the system",
  "keyword": "Given "
});
formatter.match({
  "location": "HomePageSteps.signIn()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "clicks on My tasks link",
  "keyword": "When "
});
formatter.match({
  "location": "HomePageSteps.clickMyTasks()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "clicks on Manage Subtasks button",
  "keyword": "And "
});
formatter.match({
  "location": "MyTasksSteps.manageSubtasks()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "don\u0027t fill description and erase due date",
  "keyword": "And "
});
formatter.match({
  "location": "SubTasksSteps.descriptionDueDateEmpty()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "clicks on subtask \u0027Add\u0027 button",
  "keyword": "When "
});
formatter.match({
  "location": "SubTasksSteps.addSubtask()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "empty subtask should not be created successfully",
  "keyword": "Then "
});
formatter.match({
  "location": "SubTasksSteps.emptySubtaskNotCreated()"
});
formatter.result({
  "error_message": "java.lang.AssertionError: Failure! Subtask was created expected [true] but found [false]\n\tat org.testng.Assert.fail(Assert.java:96)\n\tat org.testng.Assert.failNotEquals(Assert.java:776)\n\tat org.testng.Assert.assertTrue(Assert.java:44)\n\tat steps.SubTasksSteps.emptySubtaskNotCreated(SubTasksSteps.java:46)\n\tat ✽.empty subtask should not be created successfully(file:src/test/resources/features/CreateSubTask.feature:29)\n",
  "status": "failed"
});
formatter.step({
  "name": "the user clicks on Close button",
  "keyword": "And "
});
formatter.match({
  "location": "SubTasksSteps.clickOnCloseButton()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Create a new subtask with incorrect Due Date",
  "description": "",
  "keyword": "Scenario"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "the user is logged into the system",
  "keyword": "Given "
});
formatter.match({
  "location": "HomePageSteps.signIn()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "clicks on My tasks link",
  "keyword": "When "
});
formatter.match({
  "location": "HomePageSteps.clickMyTasks()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "clicks on Manage Subtasks button",
  "keyword": "And "
});
formatter.match({
  "location": "MyTasksSteps.manageSubtasks()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "type a subtask description with 5 characters",
  "keyword": "And "
});
formatter.match({
  "location": "SubTasksSteps.subtaskDescription(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "fill incorrect Due Date",
  "keyword": "And "
});
formatter.match({
  "location": "SubTasksSteps.incorrectDueDate()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "clicks on subtask \u0027Add\u0027 button",
  "keyword": "When "
});
formatter.match({
  "location": "SubTasksSteps.addSubtask()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "subtask should not be created successfully",
  "keyword": "Then "
});
formatter.match({
  "location": "SubTasksSteps.subtaskNotCreated()"
});
formatter.result({
  "error_message": "java.lang.AssertionError: Failure! Subtask was created expected [true] but found [false]\n\tat org.testng.Assert.fail(Assert.java:96)\n\tat org.testng.Assert.failNotEquals(Assert.java:776)\n\tat org.testng.Assert.assertTrue(Assert.java:44)\n\tat steps.SubTasksSteps.subtaskNotCreated(SubTasksSteps.java:36)\n\tat ✽.subtask should not be created successfully(file:src/test/resources/features/CreateSubTask.feature:39)\n",
  "status": "failed"
});
formatter.step({
  "name": "the user clicks on Close button",
  "keyword": "And "
});
formatter.match({
  "location": "SubTasksSteps.clickOnCloseButton()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "passed"
});
formatter.uri("file:src/test/resources/features/CreateTask.feature");
formatter.feature({
  "name": "Create Task",
  "description": "  Validating requirements from US#1",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Create a new task hitting Enter",
  "description": "",
  "keyword": "Scenario"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "the user is logged into the system",
  "keyword": "Given "
});
formatter.match({
  "location": "HomePageSteps.signIn()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "clicks on My tasks link",
  "keyword": "When "
});
formatter.match({
  "location": "HomePageSteps.clickMyTasks()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "type a task name with 10 characters",
  "keyword": "And "
});
formatter.match({
  "location": "MyTasksSteps.typeATaskName(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "press the Enter key",
  "keyword": "And "
});
formatter.match({
  "location": "MyTasksSteps.pressTheEnterKey()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the task should be appended on the list of created tasks",
  "keyword": "Then "
});
formatter.match({
  "location": "MyTasksSteps.taskAppended()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Create a new task by clicking Add button",
  "description": "",
  "keyword": "Scenario"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "the user is logged into the system",
  "keyword": "Given "
});
formatter.match({
  "location": "HomePageSteps.signIn()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "clicks on My tasks link",
  "keyword": "When "
});
formatter.match({
  "location": "HomePageSteps.clickMyTasks()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "type a task name with 10 characters",
  "keyword": "And "
});
formatter.match({
  "location": "MyTasksSteps.typeATaskName(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "clicks on Add button",
  "keyword": "And "
});
formatter.match({
  "location": "MyTasksSteps.clickAddButton()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the task should be appended on the list of created tasks",
  "keyword": "Then "
});
formatter.match({
  "location": "MyTasksSteps.taskAppended()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Create a new task with less than 3 characters in the name",
  "description": "",
  "keyword": "Scenario"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "the user is logged into the system",
  "keyword": "Given "
});
formatter.match({
  "location": "HomePageSteps.signIn()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "clicks on My tasks link",
  "keyword": "When "
});
formatter.match({
  "location": "HomePageSteps.clickMyTasks()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "type a task name with 2 characters",
  "keyword": "And "
});
formatter.match({
  "location": "MyTasksSteps.typeATaskName(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "clicks on Add button",
  "keyword": "And "
});
formatter.match({
  "location": "MyTasksSteps.clickAddButton()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "task should not be created successfully",
  "keyword": "Then "
});
formatter.match({
  "location": "MyTasksSteps.taskShouldNotBeCreated()"
});
formatter.result({
  "error_message": "java.lang.AssertionError: Failure! Task was created expected [true] but found [false]\n\tat org.testng.Assert.fail(Assert.java:96)\n\tat org.testng.Assert.failNotEquals(Assert.java:776)\n\tat org.testng.Assert.assertTrue(Assert.java:44)\n\tat steps.MyTasksSteps.taskShouldNotBeCreated(MyTasksSteps.java:33)\n\tat ✽.task should not be created successfully(file:src/test/resources/features/CreateTask.feature:23)\n",
  "status": "failed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Create a new task with more than 250 characters in the name",
  "description": "",
  "keyword": "Scenario"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "the user is logged into the system",
  "keyword": "Given "
});
formatter.match({
  "location": "HomePageSteps.signIn()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "clicks on My tasks link",
  "keyword": "When "
});
formatter.match({
  "location": "HomePageSteps.clickMyTasks()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "type a task name with 251 characters",
  "keyword": "And "
});
formatter.match({
  "location": "MyTasksSteps.typeATaskName(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "clicks on Add button",
  "keyword": "And "
});
formatter.match({
  "location": "MyTasksSteps.clickAddButton()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "task should not be created successfully",
  "keyword": "Then "
});
formatter.match({
  "location": "MyTasksSteps.taskShouldNotBeCreated()"
});
formatter.result({
  "error_message": "java.lang.AssertionError: Failure! Task was created expected [true] but found [false]\n\tat org.testng.Assert.fail(Assert.java:96)\n\tat org.testng.Assert.failNotEquals(Assert.java:776)\n\tat org.testng.Assert.assertTrue(Assert.java:44)\n\tat steps.MyTasksSteps.taskShouldNotBeCreated(MyTasksSteps.java:33)\n\tat ✽.task should not be created successfully(file:src/test/resources/features/CreateTask.feature:30)\n",
  "status": "failed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Validate the message on the top part",
  "description": "",
  "keyword": "Scenario"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "the user is logged into the system",
  "keyword": "Given "
});
formatter.match({
  "location": "HomePageSteps.signIn()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "clicks on My tasks link",
  "keyword": "When "
});
formatter.match({
  "location": "HomePageSteps.clickMyTasks()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "should see a message Hey Christian, this is your todo list for today:\u0027 on the top part",
  "keyword": "Then "
});
formatter.match({
  "location": "MyTasksSteps.messageForToday(String)"
});
formatter.result({
  "error_message": "java.lang.AssertionError: Failure! Wrong message on top part expected [true] but found [false]\n\tat org.testng.Assert.fail(Assert.java:96)\n\tat org.testng.Assert.failNotEquals(Assert.java:776)\n\tat org.testng.Assert.assertTrue(Assert.java:44)\n\tat steps.MyTasksSteps.messageForToday(MyTasksSteps.java:38)\n\tat ✽.should see a message Hey Christian, this is your todo list for today:\u0027 on the top part(file:src/test/resources/features/CreateTask.feature:35)\n",
  "status": "failed"
});
formatter.after({
  "status": "passed"
});
});